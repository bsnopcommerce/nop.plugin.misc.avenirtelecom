﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.AvenirTelecom.Helper
{
    public static class MethodExtension
    {
        /// <summary>
        /// use it like value.IsNullOrValue(valueToCheck)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueToCheck"></param>
        /// <returns></returns>
        public static bool IsNullOrValue(this int? value, int valueToCheck)
        {
            return (value ?? valueToCheck) == valueToCheck;
        }

        /// <summary>
        /// use it like value.IsNullOrValue(valueToCheck)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueToCheck"></param>
        /// <returns></returns>
        public static bool IsNullOrValue(this decimal? value, decimal valueToCheck)
        {
            return (value ?? valueToCheck) == valueToCheck;
        }

        public static bool IsValue(this decimal value, decimal valueToCheck)
        {
            return value == valueToCheck;
        }
    }
}
