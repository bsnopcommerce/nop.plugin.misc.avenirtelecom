﻿using Nop.Plugin.Misc.AvenirTelecom.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Plugin.Misc.AvenirTelecom.Data
{
    public class AvenirTelecomMonthlyPlanMap : EntityTypeConfiguration<AvenirTelecomMonthlyPlan>
    {
        public AvenirTelecomMonthlyPlanMap()
        {
            ToTable("ProductMonthlyPlan");

            HasKey(x => x.Id);
            Property(x => x.SubscriptionPlan);       
            
            Property(x => x.ProductAttributeId);
            Property(x => x.CreatedOnUtc);
            Property(x => x.UpdateOnUtc);

        
            //this.HasRequired(o => o.Plans)
            //.WithMany()
            //.HasForeignKey(o => o.Id);

        }
    }
}
