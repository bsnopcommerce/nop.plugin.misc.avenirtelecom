﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.AvenirTelecom.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.AvenirTelecom.Services
{
    public partial class PlanService : IPlanService
    {
        #region Field
        private readonly IRepository<AvenirTelecomMonthlyPlan> _avenirTelecomMonthlyPlanRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Plan> _planRepository;
        private readonly IRepository<ProductVariantAttributeValue> _productVariantAttributeValueRepository;

        #endregion

        #region Ctr

        public PlanService(IRepository<AvenirTelecomMonthlyPlan> avenirTelecomMonthlyPlanRepository, 
            IRepository<Product> productRepository,
            IRepository<Plan> planRepository,
            IRepository<ProductVariantAttributeValue> productVariantAttributeValueRepository)
        {
            _avenirTelecomMonthlyPlanRepository = avenirTelecomMonthlyPlanRepository;
            _productRepository = productRepository;
            _planRepository = planRepository;
            _productVariantAttributeValueRepository = productVariantAttributeValueRepository;
        }

        #endregion

        #region Methods

        public void DeletePlan(int id)
        {
            var item = _planRepository.GetById(id);

            //item.Deleted = true;

            _planRepository.Delete(item);
        }

        public bool UpdatePlan(Plan Plan)
        {
            _planRepository.Update(Plan);

            return true;
        }

        public void Insert(Plan Plan)
        {
            _planRepository.Insert(Plan);
        }

        public Plan Plan(int Id)
        {
            if (Id == 0)
                return null;

            var plan = _planRepository.GetById(Id);

            return plan;
        }

        public IList<Plan> GetPlanByMonthlyPlanId(int MonthlyPlanId)
        {
            if (MonthlyPlanId == 0)
                return null;

            var monthlyPlan = _planRepository.Table.
                Where(x => x.AvenirTelecomMonthlyPlan.Id == MonthlyPlanId).ToList();       

            return monthlyPlan;
        }

        public void DeleteAvenirTelecomMonthlyPlan(AvenirTelecomMonthlyPlan AvenirTelecomMonthlyPlan)
        {
            _avenirTelecomMonthlyPlanRepository.Delete(AvenirTelecomMonthlyPlan);
        }
        #endregion    
    


    

        //public IList<AvenirTelecomMonthlyPlan> GetMounthlyPlanAtviewSide(int ProductAttributeId)
        //{
        //    var query = (from avn in _avenirTelecomMonthlyPlanRepository.Table
        //                join pvav in _productVariantAttributeValueRepository.Table on avn.ProductAttributeId equals pvav.Id
        //                 where pvav.Id == ProductAttributeId
        //                select avn);
        //    var productVariantAttributeValues = query.ToList();
        //    return null;
        //}



    }
}