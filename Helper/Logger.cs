﻿using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace Nop.Plugin.Misc.AvenirTelecom.Helper
{
    public class Logger
    {
        private readonly string _logDir = "~/Plugins/Misc.AvenirTelecom/Log";
        private const string LogFile = "log.txt";
        private string _logFilePath;

        public Logger()
        {
            Set();
        }

        public Logger(string logDir)
        {
            _logDir = logDir;
            Set();
        }

        private void Set()
        {
            /*if (!Directory.Exists(_logDir))
                Directory.CreateDirectory(_logDir);*/

            //_logFilePath = String.Format(Path.Combine(_logDir, LogFile), DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            _logFilePath = System.Web.HttpContext.Current.Server.MapPath(Path.Combine(_logDir, LogFile));
        }

        private void LogWrite(string message)
        {
            using (var fs = new FileStream(_logFilePath, FileMode.Append))
            {
                var buffer = Encoding.UTF8.GetBytes(message);
                fs.Write(buffer, 0, buffer.Length);
            }
        }

        public void Log(string message)
        {
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("===============Event Occuurred at {0}===================", DateTime.Now.ToShortTimeString());
            sb.AppendLine();
            sb.AppendLine();
            sb.Append(message);
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("=======================================================");
            LogWrite(sb.ToString());
        }

        public void Log(Exception ex)
        {
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("===============Exception Occuurred at {0}===================", DateTime.Now.ToShortTimeString());
            sb.AppendLine();
            sb.AppendLine();

            sb.Append("Message");
            sb.AppendLine();
            sb.AppendLine("-----------");
            sb.AppendLine();
            sb.Append(ex.Message);

            sb.AppendLine();
            sb.AppendLine();


            sb.Append("StackTrace");
            sb.AppendLine();
            sb.AppendLine("-----------");
            sb.AppendLine();
            sb.Append(ex.StackTrace);
            sb.AppendLine();
            sb.AppendLine();

            sb.Append("===============End===================");

            LogWrite(sb.ToString());
        }
    }
}
