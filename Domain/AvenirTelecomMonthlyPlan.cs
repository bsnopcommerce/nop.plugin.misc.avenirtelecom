using System;
using Nop.Core;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Misc.AvenirTelecom.Domain
{
    public class AvenirTelecomMonthlyPlan : BaseEntity
    {
        public int ProductId { get; set; }
        [Required]
        public string SubscriptionPlan { get; set; }         
        public int? ProductAttributeId { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdateOnUtc { get; set; }       
        public virtual ICollection<Plan> Plans { get; set; }

    }

    public enum SubscriptionType
    {
        OneYearSubscription = 1,
        TwoYearSubscription = 2,
        //WithOutSubscription = 0
    }

    public enum PriceType
    {
        Cash = 1,
        Leash = 2
    }
    public class Plan : BaseEntity
    {
        [Required]
        public SubscriptionType SubscriptionType { get; set; }

        [Required]
        public string SubscriptionPlan { get; set; }
        [Required]
        public PriceType PriceType { get; set; }
        public decimal MonthlyFeeForSubscription { get; set; }
        public decimal CashPriceForDevice { get; set; }
        public decimal PricePaymentDevice { get; set; }    
        public int? ProductVariantAttributeValueId { get; set; }
        public int TopicId { get; set; }
        public AvenirTelecomMonthlyPlan AvenirTelecomMonthlyPlan { get; set; }

    }
 

}