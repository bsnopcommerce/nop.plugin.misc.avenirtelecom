﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.AvenirTelecom.Domain;
using System.Collections.Generic;
using Nop.Core.Domain.Common;

namespace Nop.Plugin.Misc.AvenirTelecom.Services
{
    public partial class AvenirTelecomMonthlyPlanService : IAvenirTelecomMonthlyPlanService
    {
        #region Field
        private readonly IRepository<AvenirTelecomMonthlyPlan> _avenirTelecomMonthlyPlanRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Plan> _planRepository;
        private readonly IRepository<ProductVariantAttributeValue> _productVariantAttributeValueRepository;
        private readonly IRepository<ProductVariantAttribute> _productVarientAttributeRepository;
        private readonly IRepository<GenericAttribute> _genericAttributeRepository;



        #endregion

        #region Ctr

        public AvenirTelecomMonthlyPlanService(IRepository<AvenirTelecomMonthlyPlan> avenirTelecomMonthlyPlanRepository, 
            IRepository<Product> productRepository,
            IRepository<Plan> planRepository,
            IRepository<ProductVariantAttribute> productVarientAttributeRepository,
            IRepository<ProductVariantAttributeValue> productVariantAttributeValueRepository,
            IRepository<GenericAttribute> genericAttributeRepository)
        {
            _avenirTelecomMonthlyPlanRepository = avenirTelecomMonthlyPlanRepository;
            _productRepository = productRepository;
            _planRepository = planRepository;
            _productVarientAttributeRepository = productVarientAttributeRepository;
             _productVariantAttributeValueRepository = productVariantAttributeValueRepository;
             _genericAttributeRepository = genericAttributeRepository;
        }

        #endregion

        #region Methods

        public void Delete(int id)
        {
            var item = _avenirTelecomMonthlyPlanRepository.GetById(id);

            //item.Deleted = true;

            _avenirTelecomMonthlyPlanRepository.Delete(item);
        }

        public bool UpdateAvenirTelecomMonthlyPlan(AvenirTelecomMonthlyPlan HomePageProductCategoryImage)
        {
            _avenirTelecomMonthlyPlanRepository.Update(HomePageProductCategoryImage);

            return true;
        }

        public bool PlanExist(int productId = 0)
        {
            if (productId > 0)
            {
                return
                    _avenirTelecomMonthlyPlanRepository.Table.Any(plan => plan.ProductId.Equals(productId));
            }

            return false;
        }

        public void Insert(AvenirTelecomMonthlyPlan item)
        {
            //default value
            item.CreatedOnUtc= DateTime.UtcNow;
            item.UpdateOnUtc = DateTime.UtcNow;

            _avenirTelecomMonthlyPlanRepository.Insert(item);
        }

        public AvenirTelecomMonthlyPlan AvenirTelecomMonthlyPlan(int Id)
        {
            if (Id == 0)
                return null;

            return _avenirTelecomMonthlyPlanRepository.GetById(Id);
        }
        public ProductVariantAttribute GetEntityByProductAttributeId(int productId, int productAttributeId)
        {
            var entity = _productVarientAttributeRepository.Table.Where(x => x.ProductAttributeId == productAttributeId && x.ProductId== productId).SingleOrDefault();
            return entity;
        }
        public IList<AvenirTelecomMonthlyPlan> GetMounthlyPlanByProductId(int ProductId)
        {
            if (ProductId == 0)
                return null;      

            var monthlyPlan = _avenirTelecomMonthlyPlanRepository.Table.
                Where(x => x.ProductId == ProductId).ToList();       

            return monthlyPlan;
        }
        public void DeleteAvenirTelecomMonthlyPlan(AvenirTelecomMonthlyPlan AvenirTelecomMonthlyPlan)
        {
            _avenirTelecomMonthlyPlanRepository.Delete(AvenirTelecomMonthlyPlan);
        }
        #endregion    
    

  

        public IList<AvenirTelecomMonthlyPlan> GetMounthlyPlanAtviewSide(int ProductAttributeId)
        {
            var query = (from avn in _avenirTelecomMonthlyPlanRepository.Table
                        join pvav in _productVariantAttributeValueRepository.Table on avn.ProductAttributeId equals pvav.Id
                         where pvav.Id == ProductAttributeId
                        select avn);
            var productVariantAttributeValues = query.ToList();
            return null;
        }



        public List<GenericAttribute> GetAttributesBykeyGroup(string key)
        {
            var GenericAttribute= _genericAttributeRepository.Table.Where(x => x.Key == key).ToList();
            return GenericAttribute;
        }
    }
}