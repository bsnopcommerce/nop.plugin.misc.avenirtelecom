﻿using Nop.Plugin.Misc.AvenirTelecom.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.AvenirTelecom.Models
{
    public class IndividualPlanViewModel
    {
        public int ProductId { get; set; }       
        public SubscriptionType SubscriptionType { get; set; }
        [Required]
        public string SubscriptionPlan { get; set; }
        [Required]
        public PriceType PriceType { get; set; }
        public decimal MonthlyFeeForSubscription { get; set; }
        public decimal CashPriceForDevice { get; set; }
        public decimal PricePaymentDevice { get; set; }
        public int? ProductVariantAttributeValueId { get; set; }

        public string ProductName { get; set; }
    }
}
