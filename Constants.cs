﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.AvenirTelecom
{
    public sealed class Constants
    {
        public static readonly string NewService = "Нова услуга";
        public static readonly string Changeofplan = "Промяна на план";
        public static readonly string PrimaSubscription = "Прима";
        public static readonly string WithoutTrafficPlan = "Без договор";

        public static readonly string Cash = "В брой";
        public static readonly string Leash = "На лизинг";

        public static readonly string OneYearSubscription = "12 месеца";
        public static readonly string TwoYearSubscription = "24 месеца";
        public static readonly string WithoutSubscription = "Без договор";

        public static readonly string SubscriptionPlan = "План";
        public static readonly string SubscriptionType = "Абонаментен план";
        public static readonly string PlanType = "С абонамент за";
        public static readonly string PriceType = "Цена";


    }
}
