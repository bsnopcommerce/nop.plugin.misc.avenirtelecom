﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Seo;

namespace Nop.Plugin.Misc.AvenirTelecom
{
    public class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            //System.Web.Mvc.ViewEngines.Engines.Insert(0, new CustomViewEngine());
    
            #region Manage

            //ViewEngines.Engines.Add(new CustomViewEngine());
            //routes.MapGenericPathRoute("Plugin.Misc.AvenirTelecom.GenericUrl", "{generic_se_name}",
            //          new
            //          {
            //              controller = "ProductPlugin", 
            //              action = "ProductDetails" 
            //          },
            //          new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" });


            var route = routes.CustomMapLocalizedRoute("Plugin.Misc.AvenirTelecom.ShoppingCartUrl",
            "cart/",
            new { controller = "ShoppingCart", action = "Cart" },
            new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" });
            routes.Remove(route);
            routes.Insert(0, route);

            var routeDirectGoToCartPage = routes.MapLocalizedRoute("Plugin.Misc.AvenirTelecom.AddProductToCart-Details",
                         "addproducttocart/details/{productId}/{shoppingCartTypeId}",
                         new { controller = "ShoppingCart", action = "AddProductToCart_Details" },
                         new { productId = @"\d+", shoppingCartTypeId = @"\d+" },
                         new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" });
            routes.Remove(routeDirectGoToCartPage);
            routes.Insert(0, routeDirectGoToCartPage);

            routes.CustomMapLocalizedRoute("Plugin.Misc.AvenirTelecom.AttributeChange",
           "ShoppingCart/ProductDetails_AttributeChange",
           new { controller = "ShoppingCart", action = "ProductDetails_AttributeChange" },
           new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" });

        


            routes.MapRoute("Plugin.Misc.AvenirTelecom.PlanSeedAdd", "Plugin/Misc/AvenirTelecom/PlanSeed",
           new
           {
               controller = "AvenirTelecomMonthPlan",
               action = "PlanSeed"
           },
           new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" }).DataTokens.Add("area", "admin");


           routes.MapRoute("Plugin.Misc.AvenirTelecom.IndividualPlanCreate", "Plugin/Misc/AvenirTelecom/IndividualPlanCreate/{productId}",
           new
           {
               controller = "AvenirTelecomMonthPlan",
               action = "PlanIndividualitemCreate"
           },
           new { productId = @"\d+"},
           new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" }).DataTokens.Add("area", "admin");



            routes.MapRoute("Plugin.Misc.AvenirTelecom.List", "Plugin/Misc/AvenirTelecom/List",
            new
            {
                controller = "AvenirTelecomMonthPlan",
                action = "List"
            },
            new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" }).DataTokens.Add("area", "admin");

           routes.MapRoute("Plugin.Misc.AvenirTelecom.SavePlanIndividual", "Plugin/Misc/AvenirTelecom/MonthlyPlan/AvenirTelecomMonthPlan/SavePlanForIndividualProduct",
           new
           {
               controller = "AvenirTelecomMonthPlan",
               action = "SavePlanForIndividualProduct"
           },
           new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.Misc.AvenirTelecom.CategoryImage", "Plugin/Misc/AvenirTelecom/MonthlyPlan/{Id}",
            new
            {
                controller = "AvenirTelecomMonthPlan",
                action = "MonthlyPlan"
            },
            new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.Misc.AvenirTelecom.DeleteProductPlan", "Plugin/Misc/AvenirTelecom/MonthlyPlan/Delete/{productId}",
            new
            {
                controller = "AvenirTelecomMonthPlan",
                action = "DeleteProductPlan"
            },
            new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.Misc.AvenirTelecom.SubCategoryList", "Plugin/Misc/HomePageProduct/SubCategoryList/{CategoryId}",
            new
            {
                controller = "HomePageProduct",
                action = "SubCategoryList"
            },
            new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" }).DataTokens.Add("area", "admin");


            routes.MapRoute("Plugin.Misc.AvenirTelecom.AddMonthlyPlan", "Plugin/Misc/AvenirTelecom/AddMonthlyPlan",
            new
            {
                controller = "AvenirTelecomMonthPlan",
                action = "AddMonthlyPlan"
            },
            new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.Misc.AvenirTelecom.UpdateMonthlyPlan", "Plugin/Misc/HomePageProduct/UpdateMonthlyPlan",
            new
            {
                controller = "AvenirTelecomMonthPlan",
                action = "UpdateMonthlyPlan"
            },
            new[] { "Nop.Plugin.Misc.AvenirTelecom.Controllers" }).DataTokens.Add("area", "admin");


           
            #endregion
          
        }
        
        public int Priority
        {
            get
            {
                return -10;
            }
        }

    }
}