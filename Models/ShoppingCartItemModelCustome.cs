﻿using Nop.Web.Models.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.AvenirTelecom.Models
{
    public partial class ShoppingCartItemModelCustome : ShoppingCartModel.ShoppingCartItemModel
    {
        public  string selectedService { get; set; }
        public string contract { get; set; }
        public string price { get; set; }
    }
   
}
