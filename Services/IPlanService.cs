﻿using System;
using Nop.Core;
using Nop.Plugin.Misc.AvenirTelecom.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.AvenirTelecom.Services
{
    public partial interface IPlanService
    {
        void Insert(Plan item);
        IList<Plan> GetPlanByMonthlyPlanId(int MonthlyPlanId);

        //IList<Plan> GetPlanAtviewSide(int ProductAttributeId);

        Plan Plan(int Id);

        bool UpdatePlan(Plan Plan);
        void DeletePlan(int id);
    }
}