﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Web.Framework.Kendoui;
using Nop.Plugin.Misc.AvenirTelecom.Models;
using System.Collections.Generic;
using System.Globalization;
using Nop.Services.Media;
using Nop.Plugin.Misc.AvenirTelecom.Services;
using Nop.Plugin.Misc.AvenirTelecom.Domain;
using Nop.Web.Framework.Mvc;
using Nop.Core.Caching;
using Nop.Services.Security;
using Nop.Services.Tax;
using Nop.Services.Directory;
using Nop.Core.Data;
using Nop.Plugin.Misc.AvenirTelecom.Helper;
using Nop.Services.Configuration;
using Nop.Services.Topics;
using Nop.Services.Common;

namespace Nop.Plugin.Misc.AvenirTelecom.Controllers
{
    public class AvenirTelecomMonthPlanController : Controller
    {
        #region Field

        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IWorkContext _workContext;
        private readonly CatalogSettings _catalogSettings;
        private readonly IProductService _productService;
        private readonly ILocalizationService _localizationService;
        private readonly ICategoryService _categoryService;
        private readonly IPictureService _pictureService;
        private readonly IAvenirTelecomMonthlyPlanService _avenirTelecomMonthlyPlanService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        private readonly ICacheManager _cacheManager;
        private readonly IPermissionService _permissionService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IPlanService _planService;
        private readonly IRepository<AvenirTelecomMonthlyPlan> _avenirTelecomMonthlyPlanRepository;
        private readonly IRepository<Plan> _planRepository;
        private readonly IRepository<PlanForSeed> _planSeedRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly ISettingService _settingContext;
        private readonly ITopicService _topicService;
        private readonly IRepository<ProductAttribute> _productAttributeRepository;
        private readonly IGenericAttributeService _genericAttributeService;


        #endregion

        #region Ctr

        public AvenirTelecomMonthPlanController(IDateTimeHelper dateTimeHelper,
            IWorkContext workContext,
            CatalogSettings catalogSettings,
            IProductService productService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            ICategoryService categoryService,
            IAvenirTelecomMonthlyPlanService avenirTelecomMonthlyPlanService,
            IWebHelper webHelper,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            ICacheManager cacheManager,
            IPermissionService permissionService,
            IPriceCalculationService priceCalculationService,
            ITaxService taxService,
            ICurrencyService currencyService,
            IPriceFormatter priceFormatter,
            IPlanService planService,
            IProductAttributeService productAttributeService,
            IRepository<AvenirTelecomMonthlyPlan> avenirTelecomMonthlyPlanRepository,
            IRepository<Product> productRepository,
            IRepository<Plan> planRepository,
            IRepository<PlanForSeed> planSeedRepository,
            ITopicService topicService,
            IRepository<ProductAttribute> productAttributeRepository,
        ISettingService settingContext)
        {
            _dateTimeHelper = dateTimeHelper;
            _settingContext = settingContext;
            _workContext = workContext;
            _catalogSettings = catalogSettings;
            _productService = productService;
            _localizationService = localizationService;
            _categoryService = categoryService;
            _productService = productService;
            _pictureService = pictureService;
            _avenirTelecomMonthlyPlanService = avenirTelecomMonthlyPlanService;
            _webHelper = webHelper;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _cacheManager = cacheManager;
            _permissionService = permissionService;
            _priceCalculationService = priceCalculationService;
            _planService = planService;
            _taxService = taxService;
            _currencyService = currencyService;
            _priceFormatter = priceFormatter;
            _productAttributeService = productAttributeService;
            _avenirTelecomMonthlyPlanRepository = avenirTelecomMonthlyPlanRepository;
            _productRepository = productRepository;
            _planRepository = planRepository;
            _planSeedRepository = planSeedRepository;
            _topicService = topicService;
            _productAttributeRepository = productAttributeRepository;
        }

        #endregion

        #region Methods

        public ActionResult List()
        {
            var model = new ProductListModel();
            model.SearchIncludeSubCategories = false;
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
            {
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });
            }

            return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/List.cshtml", model);
        }
        [HttpPost]
        public ActionResult ListJson(DataSourceRequest command, ProductListModel model)
        {
            var categoryIds = new List<int>() { model.CategoryId };
            model.CategoryId = model.CategoryId;
            model.SearchIncludeSubCategories = model.SearchIncludeSubCategories;

            //include subcategories
            if (model.SearchIncludeSubCategories && model.CategoryId > 0)
                categoryIds.AddRange(GetChildCategoryIds(model.CategoryId));

            var products = _productService.SearchProducts(
                categoryIds: categoryIds,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true
            );
            var gridModel = new DataSourceResult();
            gridModel.Data = products.Select(x =>
            {
                var defaultProductPicture = _pictureService.GetPicturesByProductId(x.Id).FirstOrDefault();
                return new ProductModel()
                {
                    Id = x.Id,
                    Add = 0,
                    DisableBuyButton = x.DisableBuyButton,
                    ShowOnHomePage = x.ShowOnHomePage,
                    Delete = 0,
                    ManageInventoryMethod = x.ManageInventoryMethod.GetLocalizedEnum(_localizationService, _workContext.WorkingLanguage.Id),
                    Name = x.Name,
                    OldPrice = x.OldPrice,
                    Price = x.Price,
                    Published = x.Published,
                    Sku = x.Sku,
                    StockQuantity = x.StockQuantity,
                    HasPlan = ifHasPlan(x.Id),
                    PictureThumbnailUrl = _pictureService.GetPictureUrl(defaultProductPicture, 75, true)
                };
            });
            gridModel.Total = products.TotalCount;
            //PublicPage();
            return Json(gridModel);
        }

        public ActionResult DeleteProductPlan(int productId = 0)
        {
            var allPlans = _avenirTelecomMonthlyPlanService.GetMounthlyPlanByProductId(productId);

            foreach (var plan in allPlans)
            {
                var planWithoutSubscription = plan.Plans.Where(x => x.SubscriptionType == 0).FirstOrDefault();
                if (planWithoutSubscription != null)
                {
                    var product = _productService.GetProductById(productId);
                    product.Price = planWithoutSubscription.MonthlyFeeForSubscription;
                    _productService.UpdateProduct(product);
                }
                plan.Plans.ToList().ForEach(x =>
                {
                    _planRepository.Delete(x);
                });
                _avenirTelecomMonthlyPlanService.DeleteAvenirTelecomMonthlyPlan(plan);
            }

            List<int> ProductVarientAttributeIds = new List<int>();
            List<string> AttributeValueNames = new List<string>
            {
                Constants.SubscriptionPlan,
                Constants.SubscriptionType,
                Constants.PlanType,
                Constants.PriceType
            };

            AttributeValueNames.ForEach(x =>
            {
                int id = _productAttributeRepository.Table.Where(pa => pa.Name.Equals(x)).FirstOrDefault().Id;
                var entity = _avenirTelecomMonthlyPlanService.GetEntityByProductAttributeId(productId, id);
                if (entity != null)
                {
                    ProductVarientAttributeIds.Add(entity.Id);
                    _productAttributeService.DeleteProductVariantAttribute(entity);
                }
            });

            ProductVarientAttributeIds.ForEach(x =>
            {
                var entityList = _productAttributeService.GetProductVariantAttributeValues(x);
                foreach (var s in entityList)
                {
                    _productAttributeService.DeleteProductVariantAttributeValue(s);
                }
            });

            //update product price
            try
            {


                Product objOfProduct = _productService.GetProductById(productId);
                var attributes = _genericAttributeService.GetAttributesForEntity(productId, "Product");


                objOfProduct.Price = Convert.ToDecimal(attributes[0].Value);
                _productService.UpdateProduct(objOfProduct);

                foreach (var attribute in attributes)
                {
                    _genericAttributeService.DeleteAttribute(attribute);
                }
            }
            catch (Exception ex)
            { 
            
            }

            return RedirectToAction("List", "AvenirTelecomMonthPlan", new { area = "" });
        }



        public ActionResult MonthlyPlan(int Id)
        {
            AvenirTelecomMonthlyPlanModel model = new AvenirTelecomMonthlyPlanModel();
            model.ProductName = _productService.GetProductById(Id).Name;
            model.ProductId = Id;

            model.SubscriptionPlan = "";
            model.AvailableSubscriptionType.Add(new SelectListItem { Text = Constants.TwoYearSubscription, Value = "1" });
            model.AvailableSubscriptionType.Add(new SelectListItem { Text = Constants.OneYearSubscription, Value = "2" });
            model.AvailableSubscriptionType.Add(new SelectListItem { Text = Constants.WithoutSubscription, Value = "3" });

            model.AvailablePriceType.Add(new SelectListItem { Text = Constants.Cash, Value = "1" });
            model.AvailablePriceType.Add(new SelectListItem { Text = _localizationService.GetResource("plugin.avenir.pricetype.Leasing"), Value = "2" });

            var isExist = _avenirTelecomMonthlyPlanService.GetMounthlyPlanByProductId(model.ProductId);
            if (isExist.Count == 0)
            {
                AddMonthlyPlan(model.ProductId);
            }


            return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/MonthlyPlan.cshtml", model);
        }
        public void AddMonthlyPlan(int ProductId)
        {
            if (ProductId == 0)
                throw new ArgumentException();

            Product objOfProduct = _productService.GetProductById(ProductId);
            List<int> ProductVariantAttributeIdAfterInser = new List<int>();

            var attributes = new Dictionary<string, int>
            {
                { Constants.SubscriptionPlan,2}, { Constants.SubscriptionType,1}, { Constants.PlanType,2}, { Constants.PriceType,1}
            };

            foreach (string s in attributes.Keys)
            {
                var productAttribute = _productAttributeService.GetAllProductAttributes().Where(x => x.Name == s).FirstOrDefault();
                if (productAttribute != null)
                {
                    var productAttr = _avenirTelecomMonthlyPlanService.GetEntityByProductAttributeId(ProductId, productAttribute.Id);
                    if (productAttr == null)
                    {
                        var pva = new ProductVariantAttribute()
                        {
                            ProductId = ProductId,
                            ProductAttributeId = productAttribute.Id,
                            TextPrompt = "",
                            IsRequired = true,
                            AttributeControlTypeId = attributes[s]
                        };
                        _productAttributeService.InsertProductVariantAttribute(pva);

                        if (s == Constants.SubscriptionPlan)
                        {
                            InsertMonthlyPlanForProduct(pva.Id, ProductId, objOfProduct);
                        }
                        else if (s == Constants.SubscriptionType)
                        {
                            InsertSubscriptionTypes(pva.Id, ProductId);
                        }
                        else if (s == Constants.PlanType)
                        {
                            InsertPlanTypes(pva.Id, ProductId);
                        }
                        else if (s == Constants.PriceType)
                        {
                            InsertPriceTypes(pva.Id, ProductId);
                        }
                    }
                }
            }

        }
        [HttpPost]
        public ActionResult MonthlyPlanList(DataSourceRequest command, int productId)
        {
            //a vendor should have access only to his products
            ViewBag.ProductName = _productService.GetProductById(productId).Name;
            var monthlyPlan = _avenirTelecomMonthlyPlanService.GetMounthlyPlanByProductId(productId);

            var data = monthlyPlan.Select(x => new
            {
                x.Id,
                x.SubscriptionPlan,
                x.ProductId,
                x.ProductAttributeId,
                Plans = x.Plans.
                Select(y => new
                {
                    Id = y.Id,
                    SubscriptionType = y.SubscriptionType,
                    PriceType = y.PriceType,
                    ProductVariantAttributeValueId = y.ProductVariantAttributeValueId,
                    CashPriceForDevice = y.CashPriceForDevice == 0 ? y.PricePaymentDevice : y.CashPriceForDevice,
                    MonthlyFeeForSubscription = y.MonthlyFeeForSubscription,
                    TotalFeesPerMonthForSubscription = y.PricePaymentDevice
                }).ToList()

            }).ToList();


            var AvenirTelecomMonthlyPlanModel = new List<AvenirTelecomMonthlyPlanModel>();


            for (int i = 0; i < data.Count; i++)
            {

                for (int j = 0; j < data[i].Plans.Count; j++)
                {
                    //string priceType= j%2==0 ? "Cash":"Leash";
                    AvenirTelecomMonthlyPlanModel.Add(new AvenirTelecomMonthlyPlanModel
                    {
                        Id = data[i].Plans[j].Id,
                        SubscriptionPlan = _productAttributeService.GetProductVariantAttributeValueById(Convert.ToInt16(data[i].Plans[j].ProductVariantAttributeValueId)).Name,
                        ProductAttributeValueId = (int)data[i].Plans[j].ProductVariantAttributeValueId,
                        SubscriptionType = data[i].Plans[j].SubscriptionType == 0 ? Constants.WithoutSubscription : data[i].Plans[j].SubscriptionType.ToString(),
                        CashPriceForDevice = data[i].Plans[j].CashPriceForDevice,
                        MonthlyFeeForSubscription = data[i].Plans[j].MonthlyFeeForSubscription,
                        SubscriptionTypeId = data[i].Plans[j].SubscriptionType.ToString() == SubscriptionType.TwoYearSubscription.ToString() ? 2 : 1,
                        PriceTypeId = data[i].Plans[j].PriceType.ToString() == Constants.Cash ? 1 : 2,
                        PriceTypeName = data[i].Plans[j].PriceType.ToString(),
                        SetAsDefaultPlan = _productAttributeService.GetProductVariantAttributeValueById(Convert.ToInt16(data[i].Plans[j].ProductVariantAttributeValueId)).IsPreSelected
                    });
                }
            }

            var gridModel = new DataSourceResult
            {
                Data = AvenirTelecomMonthlyPlanModel,
                Total = AvenirTelecomMonthlyPlanModel.Count
            };

            _settingContext.SetSetting("catalogsettings.dynamicpriceupdateajax", "True");
            _settingContext.SetSetting("catalogsettings.enabledynamicpriceupdate", "True");

            return Json(gridModel);
        }
        [HttpPost]
        public ActionResult DeleteMonthlyPlan(AvenirTelecomMonthlyPlanModel AvenirTelecomMonthlyPlanModel)
        {

            ProductVariantAttributeValue objOfProductVariantAttributeValue = _productAttributeService.GetProductVariantAttributeValueById(AvenirTelecomMonthlyPlanModel.ProductAttributeValueId);

            Plan plan = _planRepository.Table.Where(x => x.Id == AvenirTelecomMonthlyPlanModel.Id).FirstOrDefault();

            _planRepository.Delete(plan);
            //var avenirTelecomMonthlyPlan = _avenirTelecomMonthlyPlanService.AvenirTelecomMonthlyPlan(id);
            //if (avenirTelecomMonthlyPlan == null)
            //    throw new ArgumentException("No category picture found with the specified id");


            //_avenirTelecomMonthlyPlanService.DeleteAvenirTelecomMonthlyPlan(avenirTelecomMonthlyPlan);

            return new NullJsonResult();
        }
        [HttpPost]
        public ActionResult UpdateMonthlyPlan(AvenirTelecomMonthlyPlanModel AvenirTelecomMonthlyPlanModel)
        {
            ProductVariantAttributeValue objOfProductVariantAttributeValue = _productAttributeService.GetProductVariantAttributeValueById(AvenirTelecomMonthlyPlanModel.ProductAttributeValueId);

            var allAttributeValues = _productAttributeService.GetProductVariantAttributeValues(objOfProductVariantAttributeValue.ProductVariantAttributeId);

            foreach (var attributeValue in allAttributeValues)
            {
                attributeValue.IsPreSelected = false;
                _productAttributeService.UpdateProductVariantAttributeValue(attributeValue);
            }

            objOfProductVariantAttributeValue.IsPreSelected = AvenirTelecomMonthlyPlanModel.SetAsDefaultPlan;

            var price = _genericAttributeService.GetAttributesForEntity(objOfProductVariantAttributeValue.AssociatedProductId, "Product").FirstOrDefault(e => e.Key == "ProductPlanPrice");
            var formatedPrice = _genericAttributeService.GetAttributesForEntity(objOfProductVariantAttributeValue.AssociatedProductId, "Product").FirstOrDefault(e => e.Key == "FormatedProductPlanPrice");

            Plan plan = _planRepository.Table.FirstOrDefault(x => x.Id == AvenirTelecomMonthlyPlanModel.Id);

            if (AvenirTelecomMonthlyPlanModel.PriceTypeId == 1 && AvenirTelecomMonthlyPlanModel.SubscriptionTypeId == 2)
            {
                plan.CashPriceForDevice = Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.CashPriceForDevice);
                objOfProductVariantAttributeValue.PriceAdjustment = plan.CashPriceForDevice;

                price.Value = Convert.ToString(objOfProductVariantAttributeValue.PriceAdjustment);
                _genericAttributeService.UpdateAttribute(price);

                formatedPrice.Value = _priceFormatter.FormatPrice(objOfProductVariantAttributeValue.PriceAdjustment);
                _genericAttributeService.UpdateAttribute(formatedPrice);

            }


            if (AvenirTelecomMonthlyPlanModel.PriceTypeId == 2 && AvenirTelecomMonthlyPlanModel.SubscriptionTypeId == 2)
            {
                plan.PricePaymentDevice = Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.CashPriceForDevice);
                objOfProductVariantAttributeValue.PriceAdjustment = Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.CashPriceForDevice) + Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.MonthlyFeeForSubscription);

                price.Value = Convert.ToString(objOfProductVariantAttributeValue.PriceAdjustment);
                _genericAttributeService.UpdateAttribute(price);

                formatedPrice.Value = _priceFormatter.FormatPrice(objOfProductVariantAttributeValue.PriceAdjustment);
                _genericAttributeService.UpdateAttribute(formatedPrice);
            }

            if (AvenirTelecomMonthlyPlanModel.PriceTypeId == 1 && AvenirTelecomMonthlyPlanModel.SubscriptionTypeId == 1)
            {
                plan.CashPriceForDevice = Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.CashPriceForDevice);
                objOfProductVariantAttributeValue.PriceAdjustment = plan.CashPriceForDevice;

                price.Value = Convert.ToString(objOfProductVariantAttributeValue.PriceAdjustment);
                _genericAttributeService.UpdateAttribute(price);

                formatedPrice.Value = _priceFormatter.FormatPrice(objOfProductVariantAttributeValue.PriceAdjustment);
                _genericAttributeService.UpdateAttribute(formatedPrice);

            }

            if (AvenirTelecomMonthlyPlanModel.PriceTypeId == 2 && AvenirTelecomMonthlyPlanModel.SubscriptionTypeId == 1)
            {
                plan.PricePaymentDevice = Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.CashPriceForDevice);
                objOfProductVariantAttributeValue.PriceAdjustment = Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.CashPriceForDevice) + Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.MonthlyFeeForSubscription); ;


                price.Value = Convert.ToString(objOfProductVariantAttributeValue.PriceAdjustment);
                _genericAttributeService.UpdateAttribute(price);

                formatedPrice.Value = _priceFormatter.FormatPrice(objOfProductVariantAttributeValue.PriceAdjustment);
                _genericAttributeService.UpdateAttribute(formatedPrice);
            }

            plan.MonthlyFeeForSubscription = AvenirTelecomMonthlyPlanModel.MonthlyFeeForSubscription;

            _productAttributeService.UpdateProductVariantAttributeValue(objOfProductVariantAttributeValue);

            _planRepository.Update(plan);

            //_avenirTelecomMonthlyPlanService.UpdateAvenirTelecomMonthlyPlan(avenirTelecomMonthlyPlan);

            //AvenirTelecomMonthlyPlanModel model = new AvenirTelecomMonthlyPlanModel();


            //model.AvailableSubscriptionType.Add(new SelectListItem { Text = "2 years subscription", Value = "1" });
            //model.AvailableSubscriptionType.Add(new SelectListItem { Text = "1 year subscription", Value = "2" });
            //model.AvailableSubscriptionType.Add(new SelectListItem { Text = "Non subscription", Value = "3" });

            //model.AvailablePriceType.Add(new SelectListItem { Text = "Cash", Value = "1" });
            //model.AvailablePriceType.Add(new SelectListItem { Text = "Leasing", Value = "2" });

            return new NullJsonResult();
        }
        [HttpPost]
        public ActionResult GetMonthlyPlanTable(DataModel model)
        {
            //Seed();      
            var rows = _avenirTelecomMonthlyPlanService.GetMounthlyPlanByProductId(model.ProductId);
            try
            {

                var data = rows.Select(x => new
                {
                    x.Id,
                    x.SubscriptionPlan,
                    x.ProductId,
                    x.ProductAttributeId,
                    Plans = x.Plans.Where(y => (int)y.SubscriptionType == model.SubscriptionType).Where(y => (int)y.PriceType == model.PriceType).
                    Select(y => new
                    {
                        y.Id,
                        y.SubscriptionType,
                        y.SubscriptionPlan,
                        y.PriceType,
                        y.CashPriceForDevice,
                        y.MonthlyFeeForSubscription,
                        y.PricePaymentDevice,
                        SubscriptionPlanTopicId = y.TopicId == 0 ? 0 : y.TopicId,
                        pvavId = y.ProductVariantAttributeValueId
                    }).AsEnumerable().Select(z => new
                    {
                        z.Id,
                        z.SubscriptionType,
                        z.SubscriptionPlan,
                        z.PriceType,
                        CashPriceForDevice = _priceFormatter.FormatPrice(z.CashPriceForDevice),
                        MonthlyFeeForSubscription = _priceFormatter.FormatPrice(z.MonthlyFeeForSubscription),
                        PricePaymentDevice=_priceFormatter.FormatPrice(z.PricePaymentDevice),
                        TotalFeesPerMonthForDeviceAndServicePlan=_priceFormatter.FormatPrice((z.PricePaymentDevice+z.MonthlyFeeForSubscription)),
                        z.SubscriptionPlanTopicId,
                        TopicDetails = _topicService.GetTopicById(z.SubscriptionPlanTopicId).Body,
                        z.pvavId

                    }).ToList()

                }).ToList();

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult GetRegularPrice(int productId)
        {
            var monthlyPlan = _avenirTelecomMonthlyPlanService.GetMounthlyPlanByProductId(productId).Select(x => x.Plans);
            var withoutSubcribtion = monthlyPlan.FirstOrDefault().Where(x => x.SubscriptionPlan == Constants.WithoutTrafficPlan).Select(x => new
            {
                x.Id,
                pvavId = x.ProductVariantAttributeValueId,
                x.SubscriptionPlan,
                MonthlyFeeForSubscription = _priceFormatter.FormatPrice(x.MonthlyFeeForSubscription)
            });
            //var withoutSubcribtionData = _productAttributeService.GetProductVariantAttributeValueById(Convert.ToInt16(withoutSubcribtion.FirstOrDefault().ProductVariantAttributeValueId));
            return Json(withoutSubcribtion, JsonRequestBehavior.AllowGet);
        }
        //public void Seed()
        //{
        //    if (_avenirTelecomMonthlyPlanRepository.Table.Count() > 0)
        //    {
        //        return;
        //    }

        //    var monthlyPlans = new List<AvenirTelecomMonthlyPlan>
        //    {
        //        new AvenirTelecomMonthlyPlan
        //        {
        //            ProductId=15,
        //            SubscriptionPlan="Mtel endlessly 3XL",
        //            ProductAttributeId=21,
        //            CreatedOnUtc= DateTime.Now,
        //            UpdateOnUtc= DateTime.Now,
        //            Plans= new List<Plan>
        //            {
        //                new Plan
        //                {
        //                    SubscriptionType= SubscriptionType.TwoYearSubscription,
        //                    PriceType= PriceType.Cash,
        //                    CashPriceForDevice=799,
        //                    MonthlyFeeForSubscription=(decimal)69.99,
        //                    PricePaymentDevice=0,
        //                },
        //                  new Plan
        //                {
        //                    SubscriptionType= SubscriptionType.TwoYearSubscription,
        //                    PriceType= PriceType.Leash,
        //                    CashPriceForDevice=0,
        //                    MonthlyFeeForSubscription=(decimal)69.99,
        //                    PricePaymentDevice=39,
        //                },
        //                    new Plan
        //                {
        //                    SubscriptionType= SubscriptionType.OneYearSubscription,
        //                    PriceType= PriceType.Cash,
        //                    CashPriceForDevice=999,
        //                    MonthlyFeeForSubscription=(decimal)99.99,
        //                    PricePaymentDevice=0,
        //                },
        //                      new Plan
        //                {
        //                    SubscriptionType= SubscriptionType.OneYearSubscription,
        //                    PriceType= PriceType.Leash,
        //                    CashPriceForDevice=0,
        //                    MonthlyFeeForSubscription=(decimal)99.99,
        //                    PricePaymentDevice=59,
        //                }
        //            }

        //        }
        //    };

        //    monthlyPlans.ForEach(x =>
        //    {
        //        _avenirTelecomMonthlyPlanRepository.Insert(x);
        //    });


        //}
        public void InsertSubscriptionTypes(int id, int productId)
        {
            var subsTypes = new List<ProductVariantAttributeValue>
                {
                    new ProductVariantAttributeValue()
                    {
                        ProductVariantAttributeId = id,
                        AttributeValueTypeId = 0,
                        AssociatedProductId = productId,
                        Name = Constants.TwoYearSubscription,
                        ColorSquaresRgb = null,
                        PriceAdjustment =0,
                        WeightAdjustment = 0,
                        Cost = 0,
                        Quantity = 0,
                        IsPreSelected = true,
                        DisplayOrder = 0,
                        PictureId = 0,
                    },
                        new ProductVariantAttributeValue()
                    {
                        ProductVariantAttributeId =id,
                        AttributeValueTypeId = 0,
                        AssociatedProductId = productId,
                        Name = Constants.OneYearSubscription,
                        ColorSquaresRgb = null,
                        PriceAdjustment = 0,
                        WeightAdjustment = 0,
                        Cost = 0,
                        Quantity = 0,
                        IsPreSelected = false,
                        DisplayOrder = 0,
                        PictureId = 0,
                    },
                        new ProductVariantAttributeValue()
                    {
                        ProductVariantAttributeId =id,
                        AttributeValueTypeId = 0,
                        AssociatedProductId = productId,
                        Name = Constants.WithoutSubscription,
                        ColorSquaresRgb = null,
                        PriceAdjustment =0,
                        WeightAdjustment = 0,
                        Cost = 0,
                        Quantity = 0,
                        IsPreSelected = false,
                        DisplayOrder = 0,
                        PictureId = 0,
                    }
                };
            subsTypes.ForEach(x =>
            {
                _productAttributeService.InsertProductVariantAttributeValue(x);
            });
        }
        public void InsertPlanTypes(int id, int productId)
        {
            var planTypes = new List<ProductVariantAttributeValue>
                {
                    new ProductVariantAttributeValue()
                    {
                        ProductVariantAttributeId = id,
                        AttributeValueTypeId = 0,
                        AssociatedProductId = productId,
                        Name = Constants.NewService,
                        ColorSquaresRgb = null,
                        PriceAdjustment =0,
                        WeightAdjustment = 0,
                        Cost = 0,
                        Quantity = 0,
                        IsPreSelected = true,
                        DisplayOrder = 0,
                        PictureId = 0,
                    },
                        new ProductVariantAttributeValue()
                    {
                        ProductVariantAttributeId =id,
                        AttributeValueTypeId = 0,
                        AssociatedProductId = productId,
                        Name = Constants.Changeofplan,
                        ColorSquaresRgb = null,
                        PriceAdjustment = 0,
                        WeightAdjustment = 0,
                        Cost = 0,
                        Quantity = 0,
                        IsPreSelected = false,
                        DisplayOrder = 0,
                        PictureId = 0,
                    },
                        new ProductVariantAttributeValue()
                    {
                        ProductVariantAttributeId =id,
                        AttributeValueTypeId = 0,
                        AssociatedProductId = productId,
                        Name = Constants.PrimaSubscription,
                        ColorSquaresRgb = null,
                        PriceAdjustment =0,
                        WeightAdjustment = 0,
                        Cost = 0,
                        Quantity = 0,
                        IsPreSelected = false,
                        DisplayOrder = 0,
                        PictureId = 0,
                    },
                           new ProductVariantAttributeValue()
                    {
                        ProductVariantAttributeId =id,
                        AttributeValueTypeId = 0,
                        AssociatedProductId = productId,
                        Name = Constants.WithoutTrafficPlan,
                        ColorSquaresRgb = null,
                        PriceAdjustment =0,
                        WeightAdjustment = 0,
                        Cost = 0,
                        Quantity = 0,
                        IsPreSelected = false,
                        DisplayOrder = 0,
                        PictureId = 0,
                    }
                };
            planTypes.ForEach(x =>
            {
                _productAttributeService.InsertProductVariantAttributeValue(x);
            });
        }
        public void InsertPriceTypes(int id, int productId)
        {
            var priceTypes = new List<ProductVariantAttributeValue>
                {
                    new ProductVariantAttributeValue()
                    {
                        ProductVariantAttributeId = id,
                        AttributeValueTypeId = 0,
                        AssociatedProductId = productId,
                        Name = Constants.Cash,
                        ColorSquaresRgb = null,
                        PriceAdjustment =0,
                        WeightAdjustment = 0,
                        Cost = 0,
                        Quantity = 0,
                        IsPreSelected = true,
                        DisplayOrder = 0,
                        PictureId = 0,
                    },
                        new ProductVariantAttributeValue()
                    {
                        ProductVariantAttributeId =id,
                        AttributeValueTypeId = 0,
                        AssociatedProductId = productId,
                        Name = Constants.Leash,
                        ColorSquaresRgb = null,
                        PriceAdjustment = 0,
                        WeightAdjustment = 0,
                        Cost = 0,
                        Quantity = 0,
                        IsPreSelected = false,
                        DisplayOrder = 0,
                        PictureId = 0,
                    }
                };
            priceTypes.ForEach(x =>
            {
                _productAttributeService.InsertProductVariantAttributeValue(x);
            });
        }
        public void InsertMonthlyPlanForProduct(int id, int productId, Product objOfProduct)
        {
            var plans = new List<Plan>();
            var rows = _planSeedRepository.Table.ToList();
            if (rows.Count < 1)
            {
                return;
            }
            rows.ForEach(x =>
            {
                plans.Add(new Plan
                {
                    SubscriptionType = x.SubscriptionType,
                    SubscriptionPlan = x.SubscriptionPlan,
                    PriceType = x.PriceType,
                    CashPriceForDevice = x.PriceType == PriceType.Cash ? x.PricePaymentDevice : 0,
                    MonthlyFeeForSubscription = x.MonthlyFeeForSubscription,
                    PricePaymentDevice = x.PriceType == PriceType.Leash ? x.PricePaymentDevice : 0,
                    TopicId = x.TopicId,
                });
            });

            plans.Add(new Plan
            {
                SubscriptionType = 0,
                SubscriptionPlan = Constants.WithoutTrafficPlan,
                PriceType = PriceType.Cash,
                CashPriceForDevice = objOfProduct.Price,
                MonthlyFeeForSubscription = objOfProduct.Price,
                PricePaymentDevice = 0,
                TopicId = 0,
            });

            var pvavList = new List<ProductVariantAttributeValue>();
            decimal adjustmentPrice = 0;
            decimal monthlyFee = 0;
            plans.ForEach(x =>
            {

                if (x.PriceType.ToString() == PriceType.Cash.ToString() && x.SubscriptionType.ToString() == SubscriptionType.TwoYearSubscription.ToString())
                {
                    adjustmentPrice = Convert.ToDecimal(x.CashPriceForDevice);
                    monthlyFee = Convert.ToDecimal(x.MonthlyFeeForSubscription);

                }


                if (x.PriceType.ToString() == PriceType.Leash.ToString() && x.SubscriptionType.ToString() == SubscriptionType.TwoYearSubscription.ToString())
                {
                    adjustmentPrice = Convert.ToDecimal(x.PricePaymentDevice) + Convert.ToDecimal(x.MonthlyFeeForSubscription);
                    monthlyFee = Convert.ToDecimal(x.MonthlyFeeForSubscription);
                }


                if (x.PriceType.ToString() == PriceType.Cash.ToString() && x.SubscriptionType.ToString() == SubscriptionType.OneYearSubscription.ToString())
                {
                    adjustmentPrice = Convert.ToDecimal(x.CashPriceForDevice);
                    monthlyFee = Convert.ToDecimal(x.MonthlyFeeForSubscription);
                }


                if (x.PriceType.ToString() == PriceType.Leash.ToString() && x.SubscriptionType.ToString() == SubscriptionType.OneYearSubscription.ToString())
                {
                    adjustmentPrice = Convert.ToDecimal(x.PricePaymentDevice) + Convert.ToDecimal(x.MonthlyFeeForSubscription);
                    monthlyFee = Convert.ToDecimal(x.MonthlyFeeForSubscription);
                }

                if (x.SubscriptionPlan.ToString() == Constants.WithoutTrafficPlan)
                {
                    adjustmentPrice = _productService.GetProductById(productId).Price;
                }

                //if (AvenirTelecomMonthlyPlanModel.PriceTypeId == 1 && AvenirTelecomMonthlyPlanModel.SubscriptionTypeId == 1)
                //{
                //    plan.CashPriceForDevice = Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.CashPriceForDevice);
                //    objOfProductVariantAttributeValue.PriceAdjustment = plan.CashPriceForDevice;

                //}

                //if (AvenirTelecomMonthlyPlanModel.PriceTypeId == 2 && AvenirTelecomMonthlyPlanModel.SubscriptionTypeId == 1)
                //{
                //    plan.PricePaymentDevice = Convert.ToDecimal(AvenirTelecomMonthlyPlanModel.CashPriceForDevice);
                //    objOfProductVariantAttributeValue.PriceAdjustment = plan.PricePaymentDevice;

                //}


                pvavList.Add(new ProductVariantAttributeValue
                {
                    ProductVariantAttributeId = id,
                    AttributeValueTypeId = 0,
                    AssociatedProductId = productId,
                    Name = x.SubscriptionPlan,
                    ColorSquaresRgb = null,
                    PriceAdjustment = adjustmentPrice,
                    WeightAdjustment =  monthlyFee,
                    Cost = 0,
                    Quantity = 0,
                    IsPreSelected = false,
                    DisplayOrder = 0,
                    PictureId = 0
                });
            });
            pvavList.LastOrDefault().IsPreSelected = true;
            for (int i = 0; i < pvavList.Count; i++)
            {
                _productAttributeService.InsertProductVariantAttributeValue(pvavList[i]);
                plans[i].ProductVariantAttributeValueId = pvavList[i].Id;
            }

            var avenirtelecommonthlyplan = new AvenirTelecomMonthlyPlan()
            {
                ProductId = productId,
                SubscriptionPlan = rows[0].SubscriptionPlan,
                ProductAttributeId = id,
                CreatedOnUtc = DateTime.UtcNow,
                UpdateOnUtc = DateTime.UtcNow,
                Plans = plans
            };

            _avenirTelecomMonthlyPlanService.Insert(avenirtelecommonthlyplan);

            _genericAttributeService.SaveAttribute(objOfProduct, "ProductPlanPrice", objOfProduct.Price);

            _genericAttributeService.SaveAttribute(objOfProduct, "FormatedProductPlanPrice", _priceFormatter.FormatPrice(objOfProduct.Price));

            objOfProduct.Price = 0;// _productAttributeService.GetProductVariantAttributeValues(id).Where(x => x.IsPreSelected == true).FirstOrDefault().PriceAdjustment;
            _productService.UpdateProduct(objOfProduct);
        }
        //public void InsertMonthlyPlanForXL(int id, int productId, Product objOfProduct)
        //{
        //    var plans = new List<Plan>
        //    {
        //        new Plan
        //        {
        //            SubscriptionType= SubscriptionType.TwoYearSubscription,
        //            PriceType= PriceType.Cash,
        //            CashPriceForDevice=860,
        //            MonthlyFeeForSubscription=(decimal)69.99,
        //            PricePaymentDevice=0
        //        },
        //            new Plan
        //        {
        //            SubscriptionType= SubscriptionType.TwoYearSubscription,
        //            PriceType= PriceType.Leash,
        //            CashPriceForDevice=0,
        //            MonthlyFeeForSubscription=(decimal)69.99,
        //            PricePaymentDevice=(decimal)39.98
        //        },
        //            new Plan
        //        {
        //            SubscriptionType= SubscriptionType.OneYearSubscription,
        //            PriceType= PriceType.Cash,
        //            CashPriceForDevice=430,
        //            MonthlyFeeForSubscription=(decimal)34.99,
        //            PricePaymentDevice=0
        //        },
        //                new Plan
        //        {
        //            SubscriptionType= SubscriptionType.OneYearSubscription,
        //            PriceType= PriceType.Leash,
        //            CashPriceForDevice=0,
        //            MonthlyFeeForSubscription=(decimal)50,
        //            PricePaymentDevice=35
        //        }

        //    };

        //    var pvavList = new List<ProductVariantAttributeValue>
        //    {
        //        new ProductVariantAttributeValue()
        //        {
        //            ProductVariantAttributeId =id,
        //            AttributeValueTypeId = 0,
        //            AssociatedProductId = productId,
        //            Name = "Mtel endlessly XL",
        //            ColorSquaresRgb = null,
        //            PriceAdjustment =plans[0].CashPriceForDevice,
        //            WeightAdjustment = 0,
        //            Cost = 0,
        //            Quantity = 0,
        //            IsPreSelected = true,
        //            DisplayOrder = 0,
        //            PictureId = 0,
        //        },
        //         new ProductVariantAttributeValue()
        //        {
        //            ProductVariantAttributeId = id,
        //            AttributeValueTypeId = 0,
        //            AssociatedProductId = productId,
        //            Name = "Mtel endlessly XL",
        //            ColorSquaresRgb = null,
        //            PriceAdjustment = plans[1].MonthlyFeeForSubscription + plans[1].PricePaymentDevice,
        //            WeightAdjustment = 0,
        //            Cost = 0,
        //            Quantity = 0,
        //            IsPreSelected = false,
        //            DisplayOrder = 0,
        //            PictureId = 0,
        //        },
        //         new ProductVariantAttributeValue()
        //        {
        //            ProductVariantAttributeId =id,
        //            AttributeValueTypeId = 0,
        //            AssociatedProductId = productId,
        //            Name = "Mtel endlessly XL",
        //            ColorSquaresRgb = null,
        //            PriceAdjustment =plans[2].CashPriceForDevice,
        //            WeightAdjustment = 0,
        //            Cost = 0,
        //            Quantity = 0,
        //            IsPreSelected = false,
        //            DisplayOrder = 0,
        //            PictureId = 0,
        //        },
        //         new ProductVariantAttributeValue()
        //        {
        //            ProductVariantAttributeId = id,
        //            AttributeValueTypeId = 0,
        //            AssociatedProductId = productId,
        //            Name = "Mtel endlessly XL",
        //            ColorSquaresRgb = null,
        //            PriceAdjustment = plans[3].MonthlyFeeForSubscription + plans[3].PricePaymentDevice,
        //            WeightAdjustment = 0,
        //            Cost = 0,
        //            Quantity = 0,
        //            IsPreSelected = false,
        //            DisplayOrder = 0,
        //            PictureId = 0,
        //        }
        //    };

        //    for (int i = 0; i < pvavList.Count; i++)
        //    {
        //        _productAttributeService.InsertProductVariantAttributeValue(pvavList[i]);
        //        plans[i].ProductVariantAttributeValueId = pvavList[i].Id;
        //    }

        //    var avenirtelecommonthlyplan = new AvenirTelecomMonthlyPlan()
        //    {
        //        ProductId = productId,
        //        SubscriptionPlan = "Mtel endlessly XL",
        //        ProductAttributeId = id,
        //        CreatedOnUtc = DateTime.UtcNow,
        //        UpdateOnUtc = DateTime.UtcNow,
        //        Plans = plans
        //    };

        //    _avenirTelecomMonthlyPlanService.Insert(avenirtelecommonthlyplan);
        //    objOfProduct.Price = _productAttributeService.GetProductVariantAttributeValues(id).Where(x => x.IsPreSelected == true).FirstOrDefault().PriceAdjustment;
        //    _productService.UpdateProduct(objOfProduct);
        //}



        public ActionResult PlanSeedCreate()
        {
            var model = new PlanForSeed();
            var topics = _topicService.GetAllTopics(0).Where(t => t.SystemName.Contains("Avenir"));
            foreach (var topic in topics)
            {
                model.AvailableTopics.Add(new SelectListItem { Text = topic.Title, Value = topic.Id.ToString() });
            }
            return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/PlanSeedCreate.cshtml", model);
        }

        public ActionResult PlanIndividualitemCreate(int productId)
        {
            IndividualPlanViewModel objOfIndividualPlanViewModel = new IndividualPlanViewModel();
            objOfIndividualPlanViewModel.ProductId = productId;
            objOfIndividualPlanViewModel.ProductName = _productService.GetProductById(productId).Name;
            return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/IndividualPlanCreate.cshtml", objOfIndividualPlanViewModel);
        }

        [HttpPost]
        public ActionResult PlanIndividualitemCreate(IndividualPlanViewModel model)
        {

            var isPlanExist = _avenirTelecomMonthlyPlanService.GetMounthlyPlanByProductId(model.ProductId);

            if (isPlanExist.Count > 0)
            {
                var monthlyPlan = _avenirTelecomMonthlyPlanService.GetMounthlyPlanByProductId(model.ProductId)[0];

                //var monthlyPlan = _avenirTelecomMonthlyPlanService.GetMounthlyPlanByProductId(model.ProductId);

                var plan = new Plan
                {
                    SubscriptionPlan = model.SubscriptionPlan,
                    SubscriptionType = model.SubscriptionType,
                    PriceType = model.PriceType,
                    MonthlyFeeForSubscription = model.MonthlyFeeForSubscription,
                    CashPriceForDevice = model.CashPriceForDevice,
                    PricePaymentDevice = model.PricePaymentDevice,
                };
                int id = 0;
                var productAttribute = _productAttributeService.GetAllProductAttributes().Where(x => x.Name == Constants.SubscriptionPlan).SingleOrDefault();
                if (productAttribute != null)
                {
                    var productAttributeMapping = _avenirTelecomMonthlyPlanService.GetEntityByProductAttributeId(model.ProductId, productAttribute.Id);
                    if (productAttributeMapping != null)
                    {
                        id = productAttributeMapping.Id;
                    }
                }
                var productVarientAttributeValue = new ProductVariantAttributeValue
                {
                    ProductVariantAttributeId = id,
                    AttributeValueTypeId = 0,
                    AssociatedProductId = model.ProductId,
                    Name = model.SubscriptionPlan,
                    ColorSquaresRgb = null,
                    PriceAdjustment = model.PricePaymentDevice,
                    WeightAdjustment = 0,
                    Cost = 0,
                    Quantity = 0,
                    IsPreSelected = true,
                    DisplayOrder = 0,
                    PictureId = 0
                };

                _productAttributeService.InsertProductVariantAttributeValue(productVarientAttributeValue);
                plan.ProductVariantAttributeValueId = productVarientAttributeValue.Id;

                monthlyPlan.Plans.Add(plan);
                _avenirTelecomMonthlyPlanService.UpdateAvenirTelecomMonthlyPlan(monthlyPlan);

                IndividualPlanViewModel objOfIndividualPlanViewModel = new IndividualPlanViewModel();
                objOfIndividualPlanViewModel.ProductId = model.ProductId;
                objOfIndividualPlanViewModel.ProductName = _productService.GetProductById(model.ProductId).Name;

                return RedirectToAction("MonthlyPlan", "AvenirTelecomMonthPlan", new { area = "", id = model.ProductId });
            }
            else
            {
                return RedirectToAction("MonthlyPlan", "AvenirTelecomMonthPlan", new { area = "", id = model.ProductId });
            }
            //return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/IndividualPlanCreate.cshtml", objOfIndividualPlanViewModel);

        }

        [HttpPost]
        public ActionResult PlanSeedCreate(PlanForSeed model, string monthlyFeeForSubscription, string pricePaymentDevice)
        {

            if (!IsValidPlan(model))
                return RedirectToAction("PlanSeedList", "AvenirTelecomMonthPlan", new { area = "" });

            model.MonthlyFeeForSubscription = decimal.Round(decimal.Parse(monthlyFeeForSubscription, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture), 2);
            model.PricePaymentDevice = decimal.Round(decimal.Parse(pricePaymentDevice, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture), 2);

            _planSeedRepository.Insert(model);

            //return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/PlanSeedCreate.cshtml", model);
            return RedirectToAction("PlanSeedList", "AvenirTelecomMonthPlan", new { area = "" });
        }

        private bool IsValidPlan(PlanForSeed model)
        {
            if (String.IsNullOrEmpty(model.SubscriptionPlan) || !Enum.IsDefined(typeof (PriceType), model.PriceType) ||
                !Enum.IsDefined(typeof (SubscriptionType), model.SubscriptionType))
            {
                return false;
            }

            return true;
        }

        public ActionResult PlanSeedList()
        {

            return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/PlanSeed.cshtml");
        }

        [HttpPost]
        public ActionResult PlanSeedListJson(DataSourceRequest command)
        {


            var gridModel = new DataSourceResult();
            var listData = _planSeedRepository.Table.OrderBy(x => x.Id).Skip(command.Page - 1).Take(command.PageSize);
            var model = listData.ToList();
            gridModel.Data = model.Select(x =>
            {
                return new
                {
                    Id = x.Id,
                    IsActive = x.IsActive,
                    MonthlyFeeForSubscription = x.MonthlyFeeForSubscription,
                    PricePaymentDevice = x.PricePaymentDevice,
                    PriceType = Enum.GetName(typeof(PriceType), x.PriceType),
                    SubscriptionPlan = x.SubscriptionPlan,
                    SubscriptionType = Enum.GetName(typeof(SubscriptionType), x.SubscriptionType),
                    TopicId = x.TopicId
                };
            });
            gridModel.Total = model.Count;
            //PublicPage();
            return Json(gridModel);
        }


        public ActionResult PlanSeedEdit(int Id)
        {
            var model = _planSeedRepository.GetById(Id);
            var topics = _topicService.GetAllTopics(0).Where(t => t.SystemName.Contains("Avenir"));
            foreach (var topic in topics)
            {
                model.AvailableTopics.Add(new SelectListItem { Text = topic.Title, Value = topic.Id.ToString() });
            }
            return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/PlanSeedEdit.cshtml", model);
        }

        [HttpPost]
        public ActionResult PlanSeedEdit(PlanForSeed model, string monthlyFeeForSubscription, string pricePaymentDevice)
        {
            if (!IsValidPlan(model))
                return RedirectToAction("PlanSeedList", "AvenirTelecomMonthPlan", new { area = "" });

                var planForSeed = _planSeedRepository.GetById(model.Id);

                planForSeed.SubscriptionPlan = model.SubscriptionPlan;
                planForSeed.SubscriptionType = model.SubscriptionType;
                planForSeed.PriceType = model.PriceType;
                planForSeed.MonthlyFeeForSubscription = decimal.Round(decimal.Parse(monthlyFeeForSubscription, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture), 2);
                planForSeed.TopicId = model.TopicId;
                planForSeed.PricePaymentDevice = decimal.Round(decimal.Parse(pricePaymentDevice, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture), 2);
                planForSeed.IsActive = model.IsActive;

                _planSeedRepository.Update(model);

            return RedirectToAction("PlanSeedList", "AvenirTelecomMonthPlan", new { area = "" });
        }

        public ActionResult PlanSeedDelete(int Id)
        {
            var model = _planSeedRepository.GetById(Id);
            _planSeedRepository.Delete(model);
            return RedirectToAction("PlanSeedList");
        }
        #endregion

        #region
        public ActionResult Configure()
        {
            return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/Configure.cshtml");
        }

        public ActionResult PriceHandle()
        {
            return View("~/Plugins/Misc.AvenirTelecom/Views/AvenirTelecomView/PriceHandle.cshtml");
        }
        #endregion


        #region Utility

        [NonAction]
        protected virtual List<int> GetChildCategoryIds(int parentCategoryId)
        {
            var categoriesIds = new List<int>();
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, true);
            foreach (var category in categories)
            {
                categoriesIds.Add(category.Id);
                categoriesIds.AddRange(GetChildCategoryIds(category.Id));
            }
            return categoriesIds;
        }

        [NonAction]
        private bool ifHasPlan(int id)
        {
            var test = _avenirTelecomMonthlyPlanService.PlanExist(id);
            return
                _avenirTelecomMonthlyPlanService.PlanExist(id);
        }
        #endregion

    }
}