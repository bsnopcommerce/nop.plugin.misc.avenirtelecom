﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Misc.AvenirTelecom.Data;
using Nop.Plugin.Misc.AvenirTelecom.Domain;
using Nop.Plugin.Misc.AvenirTelecom.Services;

namespace Nop.Plugin.Misc.AvenirTelecom
{
    public partial class DependencyRegister : IDependencyRegistrar
    {
        #region Field

        private const string ContextName = "nop_object_context_homepage_product";

        #endregion

        #region Register

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            //Load custom data settings
            var dataSettingsManager = new DataSettingsManager();
            var dataSettings = dataSettingsManager.LoadSettings();

            //Register custom object context
            builder.Register<IDbContext>(c => RegisterIDbContext(c, dataSettings)).Named<IDbContext>(ContextName).InstancePerHttpRequest();
            builder.Register(c => RegisterIDbContext(c, dataSettings)).InstancePerHttpRequest();

            //Register services
            builder.RegisterType<AvenirTelecomMonthlyPlanService>().As<IAvenirTelecomMonthlyPlanService>();
            builder.RegisterType<PlanService>().As<IPlanService>();
            
            //Override the repository injection
            builder.RegisterType<EfRepository<AvenirTelecomMonthlyPlan>>().As<IRepository<AvenirTelecomMonthlyPlan>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(ContextName)).InstancePerHttpRequest();
            builder.RegisterType<EfRepository<Plan>>().As<IRepository<Plan>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(ContextName)).InstancePerHttpRequest();
            builder.RegisterType<EfRepository<PlanForSeed>>().As<IRepository<PlanForSeed>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(ContextName)).InstancePerHttpRequest();


        }

        #endregion

        #region DB

        public int Order
        {
            get { return 0; }
        }

        private AvenirTelecomObjectContext RegisterIDbContext(IComponentContext componentContext,
                                                                DataSettings dataSettings)
        {
            string dataConnectionStrings;

            if (dataSettings != null && dataSettings.IsValid())
            {
                dataConnectionStrings = dataSettings.DataConnectionString;
            }
            else
            {
                dataConnectionStrings = componentContext.Resolve<DataSettings>().DataConnectionString;
            }

            return new AvenirTelecomObjectContext(dataConnectionStrings);
        }

        #endregion
    }
}