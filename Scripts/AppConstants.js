﻿var appConstants = {};

appConstants.plugin = {};

appConstants.plugin.avenir = {};

appConstants.plugin.avenir.magicConstants = {
    pleaseChoose: 'Моля, изберете',
    or: 'или',
    aSubscription: 'с абонамент за',
    andPrice: 'и цена',
    subscriptionPlan: 'План',
    cashPrice: 'Цена на устройството',
    monthlyFeeForSubscription: 'Месечна такса за абонаментен план',
    totalFeesPerMonth: 'Общо такси на месец за устройство и абонаментен план',
    withoutTariffPlan: 'Без договор'
};

appConstants.plugin.avenir.choose = {
    newService: 'Нова услуга',
    changeofplan: 'Промяна на план',
    primasubscriptionto: 'Прима'
}

appConstants.plugin.avenir.subscription = {
    oneyear: '12 месеца',
    twoyear: '24 месеца',
    without: 'Без договор'
};

appConstants.plugin.avenir.pricetype = {
    cash: 'В брой',
    leash: 'На лизинг'
};