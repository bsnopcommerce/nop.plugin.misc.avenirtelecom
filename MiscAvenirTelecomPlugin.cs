﻿using Nop.Core.Plugins;
using Nop.Plugin.Misc.AvenirTelecom.Data;
using Nop.Plugin.Misc.AvenirTelecom.Services;
using Nop.Services.Localization;
using Nop.Web.Framework.Menu;
using Nop.Web.Framework.Web;
using System.Web.Routing;
using Nop.Services.Catalog;
using Nop.Core.Domain.Catalog;
using System.Linq;
using Nop.Services.Cms;
using System;
using System.Collections.Generic;
using Nop.Services.Common;

namespace Nop.Plugin.Misc.AvenirTelecom
{
    public class MiscAvenirTelecomtPlugin : BasePlugin, IWidgetPlugin, IAdminMenuPlugin
    {
        #region Fields
        const string NAMESPACE = "Nop.Plugin.Misc.AvenirTelecom.Controllers";
        private readonly AvenirTelecomObjectContext _context;
        private readonly ILocalizationService _localizationService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IAvenirTelecomMonthlyPlanService _avenirTelecomMonthlyPlanService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IProductService _productService;


        private string HeaderLinkWidget = "header_links_after";

        #endregion

        #region Ctr

        public MiscAvenirTelecomtPlugin(AvenirTelecomObjectContext context,
            IProductAttributeService productAttributeService,
            ILocalizationService localizationService,
            IAvenirTelecomMonthlyPlanService avenirTelecomMonthlyPlanService,
            IGenericAttributeService genericAttributeService,
            IProductService productService)
        {
            _productService = productService;
            _productAttributeService = productAttributeService;
            _context = context;
            _localizationService = localizationService;
            _avenirTelecomMonthlyPlanService = avenirTelecomMonthlyPlanService;
            _genericAttributeService = genericAttributeService;
        }

        #endregion

      
        #region Install / Uninstall

        public override void Install()
        {
            //resource
            //this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.LivePersonChat.ButtonCode", "Button code(max 2000)");

            this.AddOrUpdatePluginLocaleResource("admin.avenirtelecom.monthlyplane", "Monthly Plan");
            this.AddOrUpdatePluginLocaleResource("misc.avenirtelecom.AddMonthlyPlane", "Add Monthly Plan");
            this.AddOrUpdatePluginLocaleResource("misc.avenirtelecom.setmonthlyplane", "Default Plan");
            this.AddOrUpdatePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.SubscriptionItemName", "Item Name");
            this.AddOrUpdatePluginLocaleResource("Add.MonthlyPlan", "Add Monthly Plan");
            this.AddOrUpdatePluginLocaleResource("Admin.Add", "Add");
            this.AddOrUpdatePluginLocaleResource("Admin.Save", "Save");
            this.AddOrUpdatePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.SubscriptionPlan", "Subscription Plan");
            this.AddOrUpdatePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.CashPriceForDevice", "Price Of Device");
            this.AddOrUpdatePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.MonthlyFeeForSubscription", "Monthly Fee");
            this.AddOrUpdatePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.TotalFeesPerMonthForSubscription", "Total Fees Per Month For Subscription");
            this.AddOrUpdatePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.PricePaymentDevice", "Price Payment Device");
            this.AddOrUpdatePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.SubscriptionType", "Subscription Type");
            this.AddOrUpdatePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.PriceType", "Price Type");
            this.AddOrUpdatePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.TotalFeesPerMonthForDeviceAndServicePlan", "TotalFeesPerMonth For Device And Service Plan");


            this.AddOrUpdatePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.Product.MonthlyPlan.List", "Add Monthly Plan");
            this.AddOrUpdatePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.Product.MonthlyPlan.BackToList", "Back To List");
            this.AddOrUpdatePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.DefaultSeeds.Create", "Create Seed");
            this.AddOrUpdatePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.DefaultSeeds.BackToList", "Default Seeds");
            this.AddOrUpdatePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.DefaultSeeds.List", "Default Seeds");
            this.AddOrUpdatePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.Products.AddPlan", "Add Plan");

            string[] attributes = new string[] { Constants.SubscriptionPlan, Constants.SubscriptionType, Constants.PlanType, Constants.PriceType };
            foreach (string s in attributes)
            {               
                var productAtt = new ProductAttribute()
                {
                    Name = s,
                    Description = string.Empty
                };
                _productAttributeService.InsertProductAttribute(productAtt);                
              
            }

            _context.InstallSchema();
            base.Install();
        }
        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            //_settingService.DeleteSetting<FroogleSettings>();

            this.DeletePluginLocaleResource("admin.avenirtelecom.monthlyplane");
            this.DeletePluginLocaleResource("misc.avenirtelecom.setmonthlyplane");
            this.DeletePluginLocaleResource("misc.avenirtelecom.AddMonthlyPlane");
            this.DeletePluginLocaleResource("Add.MonthlyPlan");
            this.DeletePluginLocaleResource("Admin.Add");
            this.DeletePluginLocaleResource("Admin.Save");
            this.DeletePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.SubscriptionItemName");

            this.DeletePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.SubscriptionPlan");
            this.DeletePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.CashPriceForDevice");
            this.DeletePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.MonthlyFeeForSubscription");
            this.DeletePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.TotalFeesPerMonthForSubscription");
            this.DeletePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.PricePaymentDevice");
            this.DeletePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.SubscriptionType");
            this.DeletePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.PriceType");
            this.DeletePluginLocaleResource("Admin.Misc.AvenirTelecom.Fields.TotalFeesPerMonthForDeviceAndServicePlan");

            this.DeletePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.Product.MonthlyPlan.List");
            this.DeletePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.Product.MonthlyPlan.BackToList");
            this.DeletePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.DefaultSeeds.Create");
            this.DeletePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.DefaultSeeds.BackToList");
            this.DeletePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.DefaultSeeds.List");
            this.DeletePluginLocaleResource("Admin.Plugin.Misc.AvenirTelecom.Products.AddPlan");

            string[] attributes = new string[] { Constants.SubscriptionPlan, Constants.SubscriptionType, Constants.PlanType, Constants.PriceType };
            foreach (string s in attributes)
            {
                var productAttributeExist = _productAttributeService.GetAllProductAttributes().Where(x => x.Name == s).SingleOrDefault();
               if(productAttributeExist != null)
                {
                    _productAttributeService.DeleteProductAttribute(productAttributeExist);
                }               

            }

            var genericAttrs = _avenirTelecomMonthlyPlanService.GetAttributesBykeyGroup("ProductPlanPrice");

            foreach (var attribute in genericAttrs)
            {
                var product = _productService.GetProductById(Convert.ToInt16(attribute.EntityId));
                product.Price =Convert.ToDecimal(attribute.Value);
                _productService.UpdateProduct(product);
                _genericAttributeService.DeleteAttribute(attribute);
            }

            _context.Uninstall();
            base.Uninstall();
        }

        #endregion

        #region Menu Builder

        public bool Authenticate()
        {
            return true;
        }
        #endregion

        public SiteMapNode BuildMenuItem()
        {
            var menuItem = new SiteMapNode()
            {
                Title = _localizationService.GetResource("Admin.AvenirTelecom.MonthlyPlane"),
                Url = "~/Plugin/Misc/AvenirTelecom/List",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };

            var SubMenuItem = new SiteMapNode()
            {
                Title = _localizationService.GetResource("Misc.AvenirTelecom.AddMonthlyPlane"),
                Url = "~/Plugin/Misc/AvenirTelecom/List",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };

            var SubMenuItemMonthlyPlan = new SiteMapNode()
            {
                Title = _localizationService.GetResource("Misc.AvenirTelecom.SetMonthlyPlane"),
                Url = "~/AvenirTelecomMonthPlan/PlanSeedList",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };

            menuItem.ChildNodes.Add(SubMenuItem);
            menuItem.ChildNodes.Add(SubMenuItemMonthlyPlan);

            return menuItem;
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string>
            {
                HeaderLinkWidget
            };
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "AvenirTelecomMonthPlan";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Misc.AvenirTelecom.Controllers" }, { "area", null } };
        }

        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PriceHandle";
            controllerName = "AvenirTelecomMonthPlan";
            routeValues = new RouteValueDictionary()
            {
                {"Namespaces", "Nop.Plugin.Misc.AvenirTelecom.Controllers"},
                {"area", null},
                {"widgetZone", widgetZone}
            };
        }
    }
}