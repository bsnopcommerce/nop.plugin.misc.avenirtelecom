﻿using System;
using Nop.Core;
using Nop.Plugin.Misc.AvenirTelecom.Domain;
using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Misc.AvenirTelecom.Services
{
    public partial interface IAvenirTelecomMonthlyPlanService
    {
        void Insert(AvenirTelecomMonthlyPlan item);

        //IList<AvenirTelecomMonthlyPlan> GetPageProductCategoryImage();
        //IList<int> GetPageProductCategoryImageIdByCategoryId(int categoryId);
        IList<AvenirTelecomMonthlyPlan> GetMounthlyPlanByProductId(int ProductId);

        IList<AvenirTelecomMonthlyPlan> GetMounthlyPlanAtviewSide(int ProductAttributeId);

        AvenirTelecomMonthlyPlan AvenirTelecomMonthlyPlan(int Id);

        //List<AvenirTelecomMonthlyPlan> GetHomePageProductCategoryImagesByCategoryID(int categoryId);
        bool UpdateAvenirTelecomMonthlyPlan(AvenirTelecomMonthlyPlan HomePageProductCategoryImage);

        bool PlanExist(int productId = 0);

        //string GetPageProductCategoryColor(int categoryId);
        void DeleteAvenirTelecomMonthlyPlan(AvenirTelecomMonthlyPlan AvenirTelecomMonthlyPlan);

        ProductVariantAttribute GetEntityByProductAttributeId(int productId, int productAttributeId);

        List<GenericAttribute> GetAttributesBykeyGroup(string key);

    }
}