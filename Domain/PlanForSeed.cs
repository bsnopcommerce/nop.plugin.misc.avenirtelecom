﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nop.Plugin.Misc.AvenirTelecom.Domain
{
    public class PlanForSeed : BaseEntity
    {
        public PlanForSeed()
        {
            AvailableTopics = new List<SelectListItem>();
        }
        [Required]
        public string SubscriptionPlan { get; set; }
        [Required]
        public SubscriptionType SubscriptionType { get; set; }
        [Required]
        public PriceType PriceType { get; set; }
        [Required]
        public decimal MonthlyFeeForSubscription { get; set; }
        public int TopicId { get; set; }
        [Required]
        public decimal PricePaymentDevice { get; set; }
        public bool IsActive { get; set; }

        [NotMapped]
        public IList<SelectListItem> AvailableTopics { get; set; }
    }
}
