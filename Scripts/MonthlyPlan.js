﻿var MonthPlan = (function () {
    return {
        addMonthlyPlan: function () {

            var productId = $("#ProductId").val();
            if (productId == "") {
                alert('Please select category from first page of plugin');
                return;
            }

            var model = {
                ProductId: productId,
                SubscriptionPlan:$("#SubscriptionPlan").val(),
                CashPriceForDeviceOneYear: $("#CashPriceForDeviceOneYear").val(),
                MonthlyFeeForSubscriptionOneYear: $("#MonthlyFeeForSubscriptionOneYear").val(),
                PricePaymentDeviceOneYear: $("#PricePaymentDeviceOneYear").val(),
                CashPriceForDeviceTwoYear: $("#CashPriceForDevice").val(),
                MonthlyFeeForSubscriptionTwoYear: $("#MonthlyFeeForSubscription").val(),
                PricePaymentDeviceTwoYear: $("#PricePaymentDevice").val()
            };

            console.log(model);

            $.post('/AvenirTelecomMonthPlan/AddMonthlyPlan', model, function (data) {
                console.log(data);
            });
        }
    }
})();


$(document).ready(function () {
    $('#addMonthlyPlanBtn').click(function () {
        return MonthPlan.addMonthlyPlan();
    });
});